from django.urls import reverse
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from units.forms import UnitForm
from units.models import Unit


class UnitListView(ListView):
    template_name = 'unit_list.dtl'

    model = Unit

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class UnitDetailView(DetailView):
    template_name = 'unit_detail.dtl'

    model = Unit
    slug_url_kwarg = 'unit_id'
    slug_field = 'id'


class UnitCreateView(CreateView):
    template_name = 'unit_create.dtl'
    form_class = UnitForm

    model = Unit

    def get_success_url(self):
        return reverse('units:list')


class UnitUpdateView(UpdateView):
    template_name = 'unit_update.dtl'
    form_class = UnitForm

    model = Unit
    slug_url_kwarg = 'unit_id'
    slug_field = 'id'

    def get_success_url(self):
        return reverse('units:detail', kwargs=dict(unit_id=self.object.id))


class UnitDeleteView(DeleteView):
    template_name = 'unit_update.dtl'

    model = Unit
    slug_url_kwarg = 'unit_id'
    slug_field = 'id'

    def get_success_url(self):
        return reverse('units:list')

