from django.db import models
from django.db.models import CASCADE

from buildings.models import Building
from management.models import Amenity


class UnitAmenity(Amenity):
    pass


class Unit(models.Model):
    building = models.ForeignKey(
        Building,
        on_delete=CASCADE,
        related_name='units'
    )
    name = models.CharField(max_length=64)
    rent = models.FloatField(max_length=16)
    amenities = models.ManyToManyField(
        UnitAmenity,
        blank=True,
        related_name='units'
    )

    def __str__(self):
        return f"{self.name} ({self.building})"

