from django.urls import path

from units.views import (
    UnitListView,
    UnitCreateView, UnitDetailView, UnitUpdateView, UnitDeleteView
)

app_name = 'units'

urlpatterns = [
    path('', UnitListView.as_view(), name='list'),
    path('create', UnitCreateView.as_view(), name='create'),
    path('<int:unit_id>', UnitDetailView.as_view(), name='detail'),
    path('<int:unit_id>/update', UnitUpdateView.as_view(), name='update'),
    path('<int:unit_id>/delete', UnitDeleteView.as_view(), name='delete'),
]
