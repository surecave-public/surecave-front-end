from django.forms import ModelForm

from units.models import Unit


class UnitForm(ModelForm):

    class Meta:
        model = Unit
        exclude = []
