from django.urls import reverse
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from landlords.forms import LandlordCreationForm, LandlordChangeForm
from landlords.models import Landlord


class LandlordListView(ListView):
    template_name = 'landlord_list.dtl'

    model = Landlord

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class LandlordDetailView(DetailView):
    template_name = 'landlord_detail.dtl'

    model = Landlord
    slug_url_kwarg = 'landlord_id'
    slug_field = 'id'


class LandlordCreateView(CreateView):
    template_name = 'landlord_create.dtl'
    form_class = LandlordCreationForm

    model = Landlord

    def get_success_url(self):
        return reverse('landlords:list')


class LandlordUpdateView(UpdateView):
    template_name = 'landlord_update.dtl'
    form_class = LandlordChangeForm

    model = Landlord
    slug_url_kwarg = 'landlord_id'
    slug_field = 'id'

    def get_success_url(self):
        return reverse('landlords:detail', kwargs=dict(landlord_id=self.object.id))


class LandlordDeleteView(DeleteView):
    template_name = 'landlord_update.dtl'

    model = Landlord
    slug_url_kwarg = 'landlord_id'
    slug_field = 'id'

    def get_success_url(self):
        return reverse('landlords:list')

