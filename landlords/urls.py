from django.urls import path

from landlords.views import (
    LandlordListView,
    LandlordCreateView, LandlordDetailView, LandlordUpdateView, LandlordDeleteView
)

app_name = 'landlords'

urlpatterns = [
    path('', LandlordListView.as_view(), name='list'),
    path('create', LandlordCreateView.as_view(), name='create'),
    path('<int:landlord_id>', LandlordDetailView.as_view(), name='detail'),
    path('<int:landlord_id>/update', LandlordUpdateView.as_view(), name='update'),
    path('<int:landlord_id>/delete', LandlordDeleteView.as_view(), name='delete'),
]
