from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm

from landlords.models import Landlord


class LandlordCreationForm(UserCreationForm):
    class Meta:
        model = Landlord
        fields = ['email', 'first_name', 'last_name',
                  'date_of_birth', 'phone', 'address', 'zip_code',
                  'country']


class LandlordChangeForm(ModelForm):
    class Meta:
        model = Landlord
        fields = ['email', 'first_name', 'last_name',
                  'date_of_birth', 'phone', 'address', 'zip_code',
                  'country']
        exclude = ['password']
