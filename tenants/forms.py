from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm

from tenants.models import Tenant


class TenantCreationForm(UserCreationForm):
    class Meta:
        model = Tenant
        fields = ['email', 'first_name', 'last_name',
                  'date_of_birth', 'phone', 'address', 'zip_code',
                  'country', 'lease_start_date', 'lease_end_date',
                  'unit']


class TenantChangeForm(ModelForm):
    class Meta:
        model = Tenant
        fields = ['email', 'first_name', 'last_name',
                  'date_of_birth', 'phone', 'address', 'zip_code',
                  'country', 'lease_start_date', 'lease_end_date',
                  'unit']