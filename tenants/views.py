from django.urls import reverse
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from tenants.forms import TenantCreationForm, TenantChangeForm
from tenants.models import Tenant


class TenantListView(ListView):
    template_name = 'tenant_list.dtl'

    model = Tenant

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class TenantDetailView(DetailView):
    template_name = 'tenant_detail.dtl'

    model = Tenant
    slug_url_kwarg = 'tenant_id'
    slug_field = 'id'


class TenantCreateView(CreateView):
    template_name = 'tenant_create.dtl'
    form_class = TenantCreationForm

    model = Tenant

    def get_success_url(self):
        return reverse('tenants:list')


class TenantUpdateView(UpdateView):
    template_name = 'tenant_update.dtl'
    form_class = TenantChangeForm

    model = Tenant
    slug_url_kwarg = 'tenant_id'
    slug_field = 'id'

    def get_success_url(self):
        return reverse('tenants:detail', kwargs=dict(tenant_id=self.object.id))


class TenantDeleteView(DeleteView):
    template_name = 'tenant_update.dtl'

    model = Tenant
    slug_url_kwarg = 'tenant_id'
    slug_field = 'id'

    def get_success_url(self):
        return reverse('tenants:list')

