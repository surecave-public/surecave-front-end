from django.urls import path

from tenants.views import (
    TenantListView,
    TenantCreateView, TenantDetailView, TenantUpdateView, TenantDeleteView
)

app_name = 'tenants'

urlpatterns = [
    path('', TenantListView.as_view(), name='list'),
    path('create', TenantCreateView.as_view(), name='create'),
    path('<int:tenant_id>', TenantDetailView.as_view(), name='detail'),
    path('<int:tenant_id>/update', TenantUpdateView.as_view(), name='update'),
    path('<int:tenant_id>/delete', TenantDeleteView.as_view(), name='delete'),
]
