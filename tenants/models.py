from django.db import models
from django.db.models import CASCADE

from management.models import SurecaveUser
from units.models import Unit


class Tenant(SurecaveUser):
    lease_start_date = models.DateField(
        null=True,
        blank=True
    )
    lease_end_date = models.DateField(
        null=True,
        blank=True
    )
    unit = models.ForeignKey(Unit, on_delete=CASCADE)
