from django.forms import ModelForm

from buildings.models import Building


class BuildingForm(ModelForm):

    class Meta:
        model = Building
        exclude = []
