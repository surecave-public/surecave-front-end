from django.urls import path

from buildings.views import (
    BuildingListView, BuildingDetailView, BuildingCreateView, BuildingUpdateView,
    BuildingDeleteView
)

app_name = 'buildings'

urlpatterns = [
    path('', BuildingListView.as_view(), name='list'),
    path('create', BuildingCreateView.as_view(), name='create'),
    path('<int:building_id>', BuildingDetailView.as_view(), name='detail'),
    path('<int:building_id>/update', BuildingUpdateView.as_view(), name='update'),
    path('<int:building_id>/delete', BuildingDeleteView.as_view(), name='delete'),
]
