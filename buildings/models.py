from django.db import models
from django.db.models import CASCADE

from landlords.models import Landlord
from management.models import Amenity


class BuildingAmenity(Amenity):
    pass


class Building(models.Model):
    nullable = dict(null=True, blank=True)

    landlord = models.ForeignKey(
        Landlord,
        on_delete=CASCADE,
        related_name='buildings'
    )
    name = models.CharField(max_length=128)
    address = models.CharField(
        **nullable,
        max_length=128
    )
    city = models.CharField(
        **nullable,
        max_length=64
    )
    # CharField because some zip code can contain letters
    zip_code = models.CharField(
        **nullable,
        max_length=16
    )
    country = models.CharField(
        **nullable,
        max_length=64
    )
    amenities = models.ManyToManyField(
        BuildingAmenity,
        blank=True,
        related_name='buildings'
    )

    def __str__(self):
        return f"{self.name} - {self.landlord}"
