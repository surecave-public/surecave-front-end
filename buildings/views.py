from django.urls import reverse
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from buildings.forms import BuildingForm
from buildings.models import Building


class BuildingListView(ListView):
    template_name = 'building_list.dtl'

    model = Building

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class BuildingDetailView(DetailView):
    template_name = 'building_detail.dtl'

    model = Building
    slug_url_kwarg = 'building_id'
    slug_field = 'id'


class BuildingCreateView(CreateView):
    template_name = 'building_create.dtl'
    form_class = BuildingForm

    model = Building

    def get_success_url(self):
        return reverse('buildings:list')


class BuildingUpdateView(UpdateView):
    template_name = 'building_update.dtl'
    form_class = BuildingForm

    model = Building
    slug_url_kwarg = 'building_id'
    slug_field = 'id'

    def get_success_url(self):
        return reverse('buildings:detail', kwargs=dict(building_id=self.object.id))


class BuildingDeleteView(DeleteView):
    template_name = 'building_update.dtl'

    model = Building
    slug_url_kwarg = 'building_id'
    slug_field = 'id'

    def get_success_url(self):
        return reverse('buildings:list')

