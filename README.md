# surecave-front-end

## Presentation
This project is meant to test new candidates. It can be forked or cloned.  
It is a very basic application that allow a logged in user to create Landlords, Units, Buildings and Tenants.  
The structure tries to stay very consistent with Django's official guidelines :
- Five model related apps (`buildings`, `landlords`, `management`, `tenants` and `units`)
- One setting related app : `surecave_front_end`
- In each model related app the similar structure will be found:
     - `admin.py` : Manage and configure the admin part for the app. We don't rely on it for the exercise.
     - `apps.py` : Configuration of the app
     - `forms.py` : The forms used for the different views. The form is usually based on a model and will be transmitted to the associated templates thanks to the view.
     - `models.py` : ORM models. They will define a list of models that will be mapped into the db.
     - `test.py` : A list of test related to the app. The goal of this app is front-end oriented, the tests have been skipped.
     - `urls.py` : A file that will map the different urls of the app. It is then referenced into `surecave_front_end.urls` so the different urls will be shared and available for the whole project.
     - `views.py` : The different views of the App. Any back-end and data manipulation that can be required is meant to be done here.
     - a `templates` directory where every related and required template will be found ('django-template-file.dtl')
## Goal 

The main goals of this exercise are:
- To demonstrate front-end technology proficiency
- To demonstrate communication skills
- To demonstrate initiative when few specifications are given

Given an existing project, you will be required to understand its code base, how it is working and enhance the UI/UX.
Any initiative to provide more features than what is already existing will be very appreciated.
A very basic UI is already implemented, but it act more as a skeleton than as a real base to start with. Please feel free to modify/add/delete any file within the project

A particular attention will be given to responsiveness: the project must be able to be run with both phones and usual desktop. Bootstrap4 usage is very suggested in order to reach this objective.

If any major changes are made to the project please update the Readme in order to have the project running easily.

 A particular attention will be given to details. Something that renders a very clean UI will be more appreciated than extended and unfinished/unpolished features.

## Installation

Python 3.7, Django 2.1.1
### Requirements

#### Python3.7
In order to have the project running, you'll need to have one of the latest version of python installed on you OS.
You can find Python 3.7 [here](https://www.python.org/downloads/release/python-370/)

#### Virtualenv and mkvirtualenv
Having a virtual environment in order to run different projects with different codebase is a must have.  
Our virtual environment is handled with `virtualenv` ([installation guide](https://virtualenv.pypa.io/en/latest/installation/)) and `virtualenvwrapper` ([installation guide](https://virtualenvwrapper.readthedocs.io/en/latest/install.html)) but feel free to use your own.
#### Database : Postgresql
We are using Postgresql to run our database. You can download it [here](https://www.postgresql.org/download/). The project is running with the 10.5 version.

### Running the project
#### create a virtualenv 
Create a virtual environment, specifying python version might be required if multiple versions are running.
To know the path of your python version : `which python3.7`   
`mkvirtualenv surecave-front-end —python /usr/local/bin/python3.7`

#### go into the virtualenv 
Go into your virtual environment. Every python package that will be installed will now be bound to this virtual environment.  
`workon surecave-front-end`

#### install the requirements
The projects needs some specific library in order to run properly. Be sure to install them (and to list the one your are using) with:  
`pip install -r surecave_front_end/requirements.txt`

#### create a pgsql db
`createdb surecave-front-end`  
You might need to grant a role to your current user.  
Create your current user into pgsql as a super user :  
`createuser $USER -s`

#### migrate your db
Make sure your database reflects your code.  
`python manage.py migrate`

#### generate db
`python manage.py populate_db`  
Your database will be populated with a set of data containing Buildings, Landlords, Units and Tenants.
Available users are :
- landlord_1@surecave.com
- landlord_2@surecave.com
- tenant_1@surecave.com
- tenant_2@surecave.com  

The password used is 'test'

#### Run the project
`python manage.py runserver`

Project will be running on 127.0.0.1:8000
