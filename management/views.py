from django.contrib.auth.views import LoginView
from django.urls import reverse
from django.views.generic.base import TemplateView


class HomeView(TemplateView):
    template_name = 'home.dtl'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['is_anonymous'] = self.request.user.is_anonymous
        return context

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class SurecaveLoginView(LoginView):
    template_name = 'login.dtl'

    def get_success_url(self):
        return reverse('management:home')

