from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models

from management.manager import SurecaveUserManager


class SurecaveUser(AbstractBaseUser, PermissionsMixin):
    """
    Overriding BaseUser to allow authentication with email
    SurecaveUser will also have optional attributes such as:
    first name, last name, date of birth, phone, address, city,
    zip code and country
    """
    # dict that will be used to set nullable attributes of a field
    nullable = dict(null=True, blank=True)

    # attributes of the model
    email = models.EmailField(unique=True)
    first_name = models.CharField(
        **nullable,
        max_length=32
    )
    last_name = models.CharField(
        **nullable,
        max_length=32
    )
    date_of_birth = models.DateField(**nullable)
    phone = models.CharField(
        **nullable,
        max_length=16
    )
    address = models.CharField(
        **nullable,
        max_length=128
    )
    city = models.CharField(
        **nullable,
        max_length=64
    )
    # CharField because some zip code can contain letters
    zip_code = models.CharField(
        **nullable,
        max_length=16
    )
    country = models.CharField(
        **nullable,
        max_length=64
    )
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    # define the overridden manager for this model
    objects = SurecaveUserManager()

    # set the Username field as 'email'
    USERNAME_FIELD = 'email'

    def __str__(self):
        """Display a SurecaveUser object."""
        description = ""
        if self.first_name is not None:
            description += f"{self.first_name} "
        if self.last_name is not None:
            description += f"{self.last_name} "
        description += f"<{self.email}>"
        return description


class Amenity(models.Model):
    name = models.CharField(max_length=64)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name
