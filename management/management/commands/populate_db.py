from django.core.management import BaseCommand
from django.db import transaction

from landlords.models import Landlord
from tenants.models import Tenant
from buildings.models import Building, BuildingAmenity
from units.models import Unit, UnitAmenity


class Command(BaseCommand):
    help = 'Generate test data.'

    @transaction.atomic
    def handle(self, *args, **options):

        # Generate two minimalists landlords
        print("Creating Landlords...")
        landlord_1 = Landlord.objects._create_user(
            email='landlord_1@surecave.com',
            first_name='Land',
            last_name='Lord',
            password='test'
        )
        landlord_2 = Landlord.objects._create_user(
            email='landlord_2@surecave.com',
            first_name='Lord',
            last_name='Land',
            password='test'
        )
        print("Landlords created. (2)")

        landlords = [landlord_1, landlord_2]

        # Generate buildings amenities
        print("Creating Building Amenities...")
        building_amenities = ['swimming pool', 'doorman', 'laundry', 'gym']
        for amenity in building_amenities:
            BuildingAmenity.objects.create(name=amenity)
        print(f"Building Amenities created. ({len(building_amenities)})")

        # Generate buildings for landlords
        building_list = list()
        print("Creating Buildings ...")
        for landlord in landlords:
            nb_building = 4
            for i in range(0, nb_building):
                building = Building.objects.create(
                    name=f"{landlord.last_name}'s Building #{i+1}",
                    address=f"{i*12} W {i*25} street",
                    city="New York",
                    country="US",
                    landlord=landlord
                )
                building_list.append(building)
        print(f"Buildings created. ({len(building_list)})")

        # Generate units amenities
        print("Creating Unit Amenities...")
        unit_amenities = ['A/C', 'Internet', 'Furnished', 'Pet-friendly', 'Bed']
        for amenity in unit_amenities:
            UnitAmenity.objects.create(name=amenity)
        print(f"Unit Amenities created. ({len(unit_amenities)})")

        # Generate units for each building
        print("Creating Units ...")
        unit_list = list()
        for building in building_list:
            nb_unit = 3
            for i in range(0, nb_unit):
                u = Unit.objects.create(
                    name=f"Apt {i}",
                    building=building,
                    rent=(1000 + (350.32 * i)),
                )
                unit_list.append(u)
        print(f"Units created. ({len(unit_list)})")

        # Generate Tenants
        landlord_1.refresh_from_db()
        landlord_2.refresh_from_db()
        print("Creating Tenants ...")
        tenant_1 = Tenant.objects.create(
            email='tenant_1@surecave.com',
            first_name='John',
            last_name='Doe',
            unit=landlord_1.buildings.first().units.first()
        )
        tenant_1.set_password('test')
        tenant_1.save()

        tenant_2 = Tenant.objects.create(
            email='tenant_2@surecave.com',
            first_name='Jane',
            last_name='Doe',
            unit=landlord_2.buildings.first().units.first()
        )
        tenant_2.set_password('test')
        tenant_2.save()
        tenants = [tenant_1, tenant_2]
        print(f"Tenants created. ({len(tenants)})")

        print("Database populated !")
