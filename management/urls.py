from django.urls import path

from management.views import HomeView, SurecaveLoginView
from django.contrib.auth import views

app_name = 'management'

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('login', SurecaveLoginView.as_view(), name='login'),
    path('logout', views.LogoutView.as_view(next_page='management:home'), name='logout'),
]
